--[[
 @package   Vitronic
 @filename  settings.lua
 @version   1.0
 @author    Díaz Urbaneja Víctor Eduardo Diex <victor.vector008@gmail.com>
 @date      29.05.2020 16:45:47 -04
]]--

function get_settings()
	local sql = 'select * from settings limit 1'
	local result = db:get_rows(sql)
	ui.setting_trayicon:set_active(util:is_true(result[1]['trayicon']))
	ui.setting_language:set_active_id(result[1]['language'])
	return result[1]
end
---Variable global que contiene toda la configuracion
settings = get_settings()

function get_company()
	local sql = [[
		select * from company where id_company = 1
	]]
	local result = db:get_rows(sql)
	ui.setting_name.text 	= result[1].name
	ui.setting_rif.text 	= result[1].rif
	ui.setting_address.text = result[1].address
	ui.setting_phone.text 	= result[1].phone
	ui.setting_city.text 	= result[1].city
end
get_company()

function save_company()
	local sql = [[
		update company set name = %s, rif = %s,
		address = %s, phone = %s, city = %s where id_company = 1
	]]
	local values = {
		ui.setting_name.text,
		ui.setting_rif.text,
		ui.setting_address.text,
		ui.setting_phone.text,
		ui.setting_city.text
	}
	local ok, err = db:execute(sql, values)
	if (not ok) then
		ui.setting_form_message.label = 'ERROR: '.. err
		return false
	end
	ui.setting_form_message.label = 'Empresa actualizada con exito!'
	return true
end

function save_setting()
	local sql = 'update settings set trayicon = %s, language = %s'
	local values = {
		tostring(ui.setting_trayicon:get_active()),
		ui.setting_language:get_active_id()
	}
	local ok, err = db:execute(sql, values)
	if (not ok) then
		print('ERROR')
		return false
	end
	print('Configuración actualizada con exito!')
	return true
end

function ui.setting_trayicon:on_clicked()
	if (self:get_active()) then
		statusicon:set_visible(true)
	else
		statusicon:set_visible(false)
	end
end

function ui.setting_save:on_clicked()
	local setting_page = ui.stack2:get_visible_child_name()
	if ( setting_page == 'empresa' ) then
		save_company()
	elseif ( setting_page == 'general' ) then
		save_setting()
	elseif ( setting_page == 'marcas' ) then
		print("Pagina de marcas")
	end
end

function ui.setting_cancel:on_clicked()
	ui.setting_window:hide()
end
