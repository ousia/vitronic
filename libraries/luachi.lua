--[[
          _                     _     _
         | |   _   _  __ _  ___| |__ (_)
         | |  | | | |/ _` |/ __| '_ \| |
         | |__| |_| | (_| | (__| | | | |
         |_____\__,_|\__,_|\___|_| |_|_|
Copyright (c) 2020  Díaz  Víctor  aka  (Máster Vitronic)
<vitronic2@gmail.com>   <mastervitronic@vitronic.com.ve>
]]--

require('vendor.middleclass');
root		= GLib.get_current_dir();
ds		= package.config:sub(1,1);
ml 		= require('vendor.ml');
json 		= require('vendor.json');
inifile		= require("vendor.inifile");
util		= require("libraries.utils");
base64		= require("vendor.base64");
configuration	= require("libraries.configuration");
conf		= configuration:get_conf();
local config_dir= ('%s'..ds..'vitronic'):format(GLib.get_user_config_dir())
if not util:isfile(('%s'..ds..'vitronic.db'):format(config_dir)) then
	if ( ds == '/' ) then
		os.execute( ('mkdir -p %s'):format(config_dir) )
	else
		os.execute( ('mkdir %s'):format(config_dir) )
	end
end
db_file 	= ('%s'..ds..'vitronic.db'):format(config_dir)
db		= require("libraries.database.database");
db		:open()
gettext		= require("vendor.gettext");
